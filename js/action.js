$(document).ready(function () {
    $("#register-btn").click(function (e) {
        e.preventDefault();
        $('#centralModalSuccess').modal({
            show: true
        })
        $('#centralModalSuccess').on('hidden.bs.modal', function (e) {
            window.location.href = "/html/login.html"
        })
    });
}); 